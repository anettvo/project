/* Create database */
CREATE DATABASE IF NOT EXISTS `prog2053-proj`;

/* Create tables */
CREATE TABLE `users` (
  `uid` BIGINT(8) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(128) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `userType` ENUM('admin', 'moderator', 'user') DEFAULT "user" NOT NULL,
  `picture` LONGBLOB DEFAULT NULL,
  UNIQUE (`email`),
  PRIMARY KEY (`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `posts` (
`pid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`user` BIGINT(8) NOT NULL,
`title` VARCHAR(200) NOT NULL, 
`content` VARCHAR(20000) NOT NULL,
PRIMARY KEY (`pid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `comments` (
`cid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`post` BIGINT(8) NOT NULL,
`user` BIGINT(8) NOT NULL,
`comment` VARCHAR(20000),
PRIMARY KEY (`cid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`) ON DELETE CASCADE,
FOREIGN KEY(`post`) REFERENCES posts(`pid`) ON DELETE CASCADE
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;