import { Router } from '@vaadin/router';
import './src/login.js'
import './src/main.js'
import './src/new.js'
import './src/post.js'
import './src/register.js'
import './src/posts.js'
import './src/topics.js'
import './src/contact.js'
import './src/faq.js'
import './src/users.js'

const outlet = document.getElementById('outlet');
const router = new Router(outlet);

router.setRoutes([
    { path: '/', component: 'main-page' },
    { path: '/login', component: 'login-page' },
    { path: '/register', component: 'register-page' },
    { path: '/new', component: 'new-page' },
    { path: '/post/:id', component: 'post-page' },
    { path: '/posts', component: 'posts-page' },
    { path: '/topics', component: 'topics-page' },
    { path: '/useraccount', component: 'useraccount-page' },
    { path: '/contact', component: 'contact-page' },
    { path: '/faq', component: 'faq-page' },
    { path: '/users', component: 'users-page' },
]);