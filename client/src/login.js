import { LitElement, html, css } from 'lit-element';

export class Login extends LitElement {
    constructor() {
        super();
    }

    static get styles() {
        return css`
            .container {
                font-family: sans-serif;
                border: solid 2px grey;
                border-radius: 10px;
                background: radial-gradient(circle, rgba(255,255,255,0.5928572112438726) 0%, rgba(215,215,215,1) 100%);
                width: 400px;
                height: 550px;
                text-align: center;
                top: 50%;
                left: 50%;
                position: absolute;
                transform: translate(-50%,-50%);
                box-sizing: border-box;
            }
            h1{
                margin: 1vh;
                padding: 0px 20px;
                text-align: center;
                font-size: 30px;
                font-weight: bold;
            }
            h2{
                margin: 1vh;
                margin-bottom: 5vh;
                padding: 0px 20px;
                text-align: center;
                font-size: 25px;
                font-weight: bold;
            }
            label {
                margin-bottom: 0px;
                font-size: 18px;
            }
            input {
                width: 90%;
                height: 30px;
                margin-top: 1vh;
                margin-bottom: 1vh;
                border: solid 1px black;
                border-top: solid 1px black;;    
                border-radius: 5px;
                text-align: center;
            }
            button {
                width: 50%;
                height: 40px;
                background: grey;
                color: black;
                border: solid 1px black;
                border-radius: 6px;
                margin-top: 4vh;
            }
            button:hover {
                background: grey;
                cursor: pointer;
            }
        `;
    }

    render() {
        return html`
        <div class="container">
        <h1>LOGIN</h1>
        <form>
        <div>
            <label for="email">EMAIL</label>
            <input type="email" id="email" name="email" placeholder="Write your email" required>
        </div>
        <div>
            <label for="password">PASSWORD</label>
            <input type="password" id="password" name="password" placeholder="Password" required>
        </div>
            <button type="button" @click="${this.login}">LOGIN</button>
        </form>
        </div>
        `;
    }

    login(e) {
        const formData = new FormData(e.target.form);
        var entries = {};

        for (var [key, value] of formData.entries()) {
            entries[key] = value;
        }

        const data = JSON.stringify(entries);

        fetch(`${window.MyAppGlobals.serverURL}login`, {
            method: 'POST',
            headers: {
                "content-type": "application/json; charset=UTF-8"
            },
            body: data,
            credentials: 'include'
        }).then(res => {
            if (res.ok) {
                window.location.assign("/posts");
            }
            else {
                res.json().then(data => {
                    alert(data['msg']);
                })
            }
        })
    }
}

customElements.define('login-page', Login);
