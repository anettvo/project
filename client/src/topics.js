import { LitElement, html, css } from 'lit-element';

export class Topics extends LitElement {

  constructor() {
    super();
  }

  static get styles() {
    return css`        
        body {
            background-color: #f3f8c1;
            color: rgb(12, 10, 10);
            font-size: 1.2em;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif;    
        }

        .container {
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%,-50%);
        }

        `;
  }

  render() {
    return html`
        <div class="container">
        <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Trendy.</th>
            <th scope="col">Topic</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Topic1</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Topic2</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Topic3</td>
          </tr>
        </tbody>
      </table>
      
      <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">Newest</th>
            <th scope="col">Topic</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Topic1</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Topic2</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Topic3</td>
          </tr>
        </tbody>
      </table>
      </div>

        `;
  }
}
customElements.define('topics-page', Topics);
