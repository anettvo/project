import { LitElement, html, css } from 'lit-element';

export class New extends LitElement {


    constructor() {
        super();
    }

    static get styles() {
        return css`        
        body {
            background-color: #f3f8c1;
            color: rgb(12, 10, 10);
            font-size: 1.2em;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif;    
        }

        .container {
            width: auto;
            height: auto;
            display: grid;
            grid-template-columns: auto;
            grid-template-rows: 50px auto 50px;
        }

        post {
            font-family: sans-serif;
            border: solid 2px #696969;
            border-radius: 10px;
            background: rgb(255,255,255);
            background: radial-gradient(circle, rgba(255,255,255,0.5928572112438726) 0%, rgba(215,215,215,1) 100%);
            width: 800px;
            height: 600px;
            text-align: center;
            box-sizing: border-box;
            top: 50%;
            left: 50%;
            display: grid;
            grid-column: 2;
            grid-row: 2;
        } 
        
        h1 {
            margin-top: 5vh;
        }

        label {
            text-align: center;
            font-size: 18px;
            padding: 0px 30px;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .inputTitle {
            width: 90%;
            height: 30px;
            margin-top: 1vh;
            margin-bottom: 1vh;
            border: solid 1px black;
            border-top: solid 1px black;;    
            border-radius: 5px;
            text-align: center;
        }

        .inputPost {
            width: 90%;
            height: 100px;
            margin-top: 1vh;
            margin-bottom: 1vh;
            border: solid 1px black;
            border-top: solid 1px black;;    
            border-radius: 5px;
            text-align: center;
        }

        button {
            width: 50%;
            height: 40px;
            background: grey;
            color: black;
            border: solid 1px black;
            border-radius: 6px;
            margin-top: 4vh;
        }

        button:hover {
            background: grey;
            cursor: pointer;
        }

        a{
            color: rgb(12, 10, 10);
        }
        `;
    }

    render() {
        return html`
            <div class="container">
                <post>
                    <h1>New post</h1>
                    <form>
                        <div>
                            <label for="title">Title</label>
                            <input type="text" id="title" name="title" placeholder="Title of post here" required class="inputTitle">
                        </div>
                        <div>
                            <label for="content">Main Text</label>
                            <input type="text" id="content" name="content" placeholder="Write your concern here..." required class="inputPost">
                        </div>
                            <button type="button" @click="${this.post}">Submit post</button>
                        </div>
                        
                    </form>
                </post>
            </div>
        `;
    }

    post(e) {
        const formData = new FormData(e.target.form);
        var entries = {};

        for (var [key, value] of formData.entries()) {
            entries[key] = value;
        }

        const data = JSON.stringify(entries);

        fetch(`${window.MyAppGlobals.serverURL}createPost`, {
            method: 'POST',
            headers: {
                "content-type": "application/json; charset=UTF-8"
            },
            body: data,
            credentials: 'include'
        }).then(res => {
            if (res.ok) {
                window.location.assign("/posts");
            }
            else {
                res.json().then(data => {
                    alert(data['msg']);
                    window.location.assign("/login");
                })
            }
        })

    }
}
customElements.define('new-page', New);