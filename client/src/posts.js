import { LitElement, html, css } from 'lit-element';

export class Posts extends LitElement {

  static get properties() {
    return {
      posts: { type: Array },
    };
  }

  constructor() {
    super();
    this.posts = [{ 'title': "Title", "content": "Content", "user": "User" }];
    fetch(`${window.MyAppGlobals.serverURL}getAllPosts`, {
      method: 'POST',
      headers: {
        "content-type": "application/json; charset=UTF-8"
      },
      credentials: 'include'
    }).then(res => res.json()
    ).then(data => {
      this.posts = data;
    });
  }

  static get styles() {
    return css`        
        body {
            background-color: #f3f8c1;
            color: rgb(12, 10, 10);
            font-size: 1.2em;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif;    
        }

        .container {
          text-align: center;
          padding-top: 20px;
        }

        `;
  }

  render() {

    return html`
        <div class="container">
        <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Number</th>
            <th scope="col">Title</th>
            <th scope="col">User</th>
          </tr>
        </thead>
        <tbody>
        ${this.posts.map((post, i) => html`
        <tr>
        <th scope="row">${i + 1}</th>
        <td><a href="/post/${post.pid}">${post.title}</td>
        <td>${post.user}</td>
        </tr>`
    )} 
      </tbody>
      </table>
      </div>

        `;
  }
}
customElements.define('posts-page', Posts)
