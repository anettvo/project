import { LitElement, html, css } from 'lit-element';

export class Post extends LitElement {

  static get properties() {
    return {
      post: Number,
      title: String,
      content: String,
      user: String,
      comments: { type: Array }
    }
  }

  connectedCallback() {

    super.connectedCallback();

    this.post = this.location.params.id;

    fetch(`${window.MyAppGlobals.serverURL}getPost/${this.post}`, {
      method: 'POST',
      headers: {
        "content-type": "application/json; charset=UTF-8"
      },
      credentials: 'include'
    }).then(res => res.json()
    ).then(data => {
      this.title = data['title'];
      this.content = data['content'];
      this.user = data['user'];
    });

    fetch(`${window.MyAppGlobals.serverURL}getComments/${this.post}`, {
      method: 'POST',
      "headers": {
        "content-type": "application/json; charset=UTF-8"
      },
      credentials: 'include'
    }).then(res => res.json()
    ).then(data => {
      this.comments = data;
    });

  }

  constructor() {
    super();
    this.comments = [];
  }

  static get styles() {
    return css`        
        body {
            background-color: #f3f8c1;
            color: rgb(12, 10, 10);
            font-size: 1.2em;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif;    
        }

        .container {
            font-family: sans-serif;
            border: solid 2px grey;
            border-radius: 10px;
            background: radial-gradient(circle, rgba(255,255,255,0.5928572112438726) 0%, rgba(215,215,215,1) 100%);
            width: auto;
            height: auto;
            padding-bottom: 70px;
        }

        h1 {
          text-align: center;
          font-family: sans-serif;
          font-size: 30px;
          font-weight: bold;
        }

        h2 {
          margin-top: 5vh;
          margin-bottom: 2vh;
          font-family: sans-serif;
          font-size: 18px;
          font-weight: bold;
        }


        .contentPost {
          margin: auto;
          font-family: sans-serif;
          text-align: center;
          border: solid 2px grey;
          border-radius: 10px;
          background: white;
          width: 700px;
          height: auto;
          box-sizing: border-box;
          margin-top: 2vh;
          padding: 10px 10px;
        }

        .contentComment {
          margin: auto;
          font-family: sans-serif;
          text-align: center;
          border: solid 2px grey;
          border-radius: 10px;
          background: white;
          width: 700px;
          height: auto;
          box-sizing: border-box;
          margin-top: 2vh;
          margin-bottom: 0vh;
          padding: 10px 10px;
        }
        
        .username {
          padding: 10px 10px;
          font-family: sans-serif;
          font-size: 18px;
          font-weight: bold;
        }

        a {
            font-size: 2em;
        }

        form {
          text-align: center;
        }
        `;
  }

  render() {
    return html`
        <div class="container">
          <h1> ${this.title}</h1>
            <div class="username">
              User: ${this.user}
            </div>
          <div class="contentPost"> 
            ${this.content} 
          </div>
          <br>
          <form>
          <div>
          <label for="comment"></label>
          <textarea id="comment" name="comment" rows="6" cols="60" placeholder="Say something ..." required></textarea>
          <br>
          <button type="button" @click="${this.createComment}">Create comment</button>
          </form>
          </div>
          <h2> Comments: </h2>
          ${this.comments.map(comment => html`
          <div class="contentComment">
          <b>User:</b> ${comment.user} <br>
          ${comment.comment}
          </div>`
    )}
        </div>
        `;
  }

  createComment(e) {
    const formData = new FormData(e.target.form);

    let data = { comment: formData.get('comment'), post: this.post };
    console.log("asd");
    data = JSON.stringify(data);

    fetch(`${window.MyAppGlobals.serverURL}createComment`, {
      method: 'POST',
      headers: {
        "content-type": "application/json; charset=UTF-8"
      },
      body: data,
      credentials: 'include'
    }).then(res => {
      console.log("test")
      if (res.ok) {
        window.location.reload();
      }
      else {
        alert("Error, please login");
        window.location.assign("/login");
      }
    })
  }
}

customElements.define('post-page', Post);