import { LitElement, html, css } from 'lit-element';

export class Contact extends LitElement {

    constructor() {
        super();
    }

    static get styles() {
        return css`        
        div {
            width: 5000px;
            height: 420px;
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%,-50%);
            box-sizing: border-box;
            border: solid 2px grey;
            padding: 70px 30px;
            margin: 0%;
            background: rgb(255,255,255);
            background: radial-gradient(circle, rgba(255,255,255,0.5928572112438726) 0%, rgba(215,215,215,1) 100%);
            backdrop-filter: blur(5px);
            text-align: center;
            font-size: 1.2em;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif;    
        }      
        h1{
            margin: 1vh; 
            margin-bottom: 3vh;
            padding: 0px 20px;
            text-align: center;
            font-size: 50px;
            font-weight: bold;
        }
        a{
            text-align: center;
            font-size: 20px;
        }
        p{
            margin: 1vh; 
            margin-bottom: 3vh;
            padding: 0px 20px;
            text-align: center;
            font-size: 20px;
        }
        .container {
            text-align: center;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif; 
        `;
    }

    render() {
        return html`  
            <div class="container">
                <h1>Contact us</h1>
                <p> Thea Fritzvold Hatlem, <a href="mailto:thea.f.hatlem@ntnu.no?subject=Mail from the forum">send email</a></p>
                <p> Emma Rønningstad, <a href="mailto:emmaron@stud.ntnu.no?subject=Mail from the forum">send email</a></p>
                <p> Eirik Tobiassen, <a href="mailto:eiriktob@stud.ntnu.no?subject=Mail from the forum">send email</a></p>
                <p> Anett Voldheim Øverstad, <a href="mailto:anettvo@stud.ntnu.no?subject=Mail from the forum">send email</a></p>
            </div>
        `;
    }

}
customElements.define('contact-page', Contact);