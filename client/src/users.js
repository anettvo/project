import { LitElement, html, css } from 'lit-element';

export class Posts extends LitElement {

    static get properties() {
        return {
            users: { type: Array },
        };
    }

    constructor() {
        super();
        this.users = [{ uid: 'uid', "email": "Email" }];

        fetch(`${window.MyAppGlobals.serverURL}getAllUsers`, {
            method: 'POST',
            headers: {
                "content-type": "application/json; charset=UTF-8"
            },
            credentials: 'include'
        }).then(res => {
            if (res.ok) {
                res.json().then(data => {
                    this.users = data;
                });
            } else {
                res.json().then(data => {
                    window.location.assign("/login");
                    alert(data['msg']);
                });
            }
        })
    }

    static get styles() {
        return css`        
        body {
            background-color: #f3f8c1;
            color: rgb(12, 10, 10);
            font-size: 1.2em;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif;    
        }

        .container {
          text-align: center;
          padding-top: 20px;
        }

        `;
    }

    render() {

        return html`
        <div class="container">
        <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Uid</th>
            <th scope="col">Email</th>
          </tr>
        </thead>
        <tbody>
        ${this.users.map(user => html`
        <tr>
        
        <td>${user.uid}</td>
        <td>${user.email}</td>
        <form>
        <div>
            <label for="delete"></label>
            <button type="button" name=${user.uid} @click="${this.delete}">DELETE</button>
            <button type="button" name=${user.uid} @click="${this.upgradeM}">MAKE MODERATOR</button>
            <button type="button" name=${user.uid} @click="${this.upgradeA}">MAKE ADMIN</button>
        </div>
        </form>
        </tr>`

        )} 
      </tbody>
      </table>
      </div>

        `;
    }

    delete(e) {
        let data = JSON.stringify({ uid: e.srcElement.name });

        fetch(`${window.MyAppGlobals.serverURL}deleteUser`, {
            method: 'POST',
            headers: {
                "content-type": "application/json; charset=UTF-8"
            },
            body: data,
            credentials: 'include'
        }).then(res => res.json()
        ).then(data => {
            alert(data['msg']);
            window.location.reload();
        });
    }
    upgradeM(e) {
        let data = JSON.stringify({ uid: e.srcElement.name, mode: 'm' });

        fetch(`${window.MyAppGlobals.serverURL}upgradeUser`, {
            method: 'POST',
            headers: {
                "content-type": "application/json; charset=UTF-8"
            },
            body: data,
            credentials: 'include'
        }).then(res => res.json()
        ).then(data => {
            alert(data['msg']);
            window.location.reload();
        });
    }
    upgradeA(e) {
        let data = JSON.stringify({ uid: e.srcElement.name, mode: 'a' });

        fetch(`${window.MyAppGlobals.serverURL}upgradeUser`, {
            method: 'POST',
            headers: {
                "content-type": "application/json; charset=UTF-8"
            },
            body: data,
            credentials: 'include'
        }).then(res => res.json()
        ).then(data => {
            alert(data['msg']);
            window.location.reload();
        });
    }

}
customElements.define('users-page', Posts)
