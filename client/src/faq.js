import { LitElement, html, css } from 'lit-element';

export class Faq extends LitElement {

    constructor() {
        super();
    }

    static get styles() {
        return css`        
        div {
            width: 5000px;
            height: 500px;
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%,-50%);
            box-sizing: border-box;
            border: solid 2px grey;
            padding: 70px 30px;
            margin: 0%;
            background: rgb(255,255,255);
            background: radial-gradient(circle, rgba(255,255,255,0.5928572112438726) 0%, rgba(215,215,215,1) 100%);
            backdrop-filter: blur(5px);
            text-align: center;
            font-size: 1.2em;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif;    
        }      
        h1{
            margin: 1vh; 
            margin-bottom: 3vh;
            padding: 0px 10px;
            text-align: center;
            font-size: 50px;
            font-weight: bold;
        }
        h2{
            margin: 1vh;
            margin-bottom: 5vh;
            padding: 0px 20px;
            text-align: center;
            font-size: 25px;
            font-weight: bold;
        }
        h3{
            margin: 1vh;
            text-align: center;
            font-size: 20px;
            font-weight: bold;
        }
        a{
            text-align: center;
            font-size: 20px;
        }
        p{
            margin: 1vh; 
            margin-bottom: 3vh;
            padding: 0px 20px;
            text-align: center;
            font-size: 20px;
        }
        .container {
            text-align: center;
            font-family: 'Helvetics Neue', Helvetica, Arial, sans-serif; 
        `;
    }

    render() {
        return html`  
            <div class="container">
                <h1>FAQ</h1>
                <h2>Frequently asked questions</h2>
                <p>
                    <h3>How do I navigate the forum?</h3> 
                    Simply use the navigation bars on the top and bottom of the page.
                </p>
                <p>
                    <h3>What if I have problem with the forum?</h3> 
                    Click on 'Contact us' in the bottom left corner and send us an email. We'll get right on it!
                </p>
                <p>
                    <h3>How do I get in contact with you?</h3> 
                    Click on 'Contact us' in the bottom left corner and you will find our email addresses there.
                </p>
            </div>
        `;
    }

}
customElements.define('faq-page', Faq);