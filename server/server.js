"use strict";
import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const mysql = require('mysql');
const express = require('express');
const handler = require('./src/handler.cjs');
const cors = require('cors')
const cookieParser = require('cookie-parser');

async function main() {
  const db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "a-b1t-secur3",
    database: 'prog2053-proj',
    multipleStatements: false // Protect against injection
  });

  const app = express();

  app.use(express.json());
  app.use(cookieParser());

  app.use(cors({ // CORS middleware
    origin: 'http://localhost:8080',
    methods: 'POST',
    allowedHeaders: 'content-type',
    credentials: true,
  }));

  const PORT = 8081;

  app.post('/getAllPosts', handler.methods.getAllPosts(db)); // For the posts display page
  app.post('/getAllUsers', handler.methods.auth(), handler.methods.getAllUsers(db));
  app.post('/getUser', handler.methods.getUser(db));
  app.post('/upgradeUser', handler.methods.auth(), handler.methods.upgradeUser(db));
  app.post('/deleteUser', handler.methods.auth(), handler.methods.deleteUser(db));
  app.post('/createUser', handler.methods.createUser(db)); // 3 functions
  app.post('/createPost', handler.methods.auth(), handler.methods.createPost(db)); // for creating
  app.post('/createComment', handler.methods.auth(), handler.methods.createComment(db)); // user/post/comment
  app.post('/login', handler.methods.login(db)); // login handler
  app.post('/getComments/:pid', handler.methods.getComment(db)); // "Get" a set of specific comments
  app.post('/getPost/:pid', handler.methods.getPost(db)); // "Get" a specific post

  await connect(db).catch((err) => console.log('Failed to connect to DB.'));

  app.listen(PORT, () => {
    console.log('Running on port', PORT, '...');
  })
}

main();

function connect(db) {
  return new Promise((resolve, reject) => {
    db.connect(err => {
      if (err) reject(err)
      else resolve()
    })
  })
}