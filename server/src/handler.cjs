"use strict";

const fs = require('fs');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mysql = require('mysql');

// This probably isnt too safe, for production new keys should be generated, not stored in git
const privateKey = fs.readFileSync("jwtRS256.key")
const publicKey = fs.readFileSync("jwtRS256.key.pub")

const methods = {};

const saltRounds = 10;

module.exports = {
    methods
};

methods.getAllPosts = function (db) {
    return function (req, res) {
        dbQuery(db, 'SELECT * FROM posts').then(result => {
            res.status(200).send(result);
        }, reject => {
            console.log(reject);
            res.status(500).json({ 'msg': "Error in database" });
        })
    }
}

methods.getAllUsers = function (db) {
    return function (req, res) {
        if (req.auth) {
            let sql = 'SELECT userType FROM users WHERE email = ?'
            let inserts = req.auth.sub;
            dbQuery(db, sql, inserts).then(valid => {
                if (valid[0].userType == 'admin' || valid[0].userType == 'moderator') {
                    dbQuery(db, 'SELECT uid, email FROM users').then(result => {
                        res.status(200).send(result);
                    }, reject => {
                        console.log(reject);
                        res.status(500).json({ 'msg': "Error in database" });
                    })
                }
                else {
                    res.status(401).json({ 'msg': 'You are not authorized to do see all users' });
                }
            })
        }
        else {
            res.status(401).json({ 'msg': "You are not logged in" });
        }
    }
}

methods.getComment = function (db) {
    return function (req, res) {
        let pid = req.params.pid;
        let sql = "SELECT user, comment FROM comments WHERE post = ?";
        let inserts = [pid];

        dbQuery(db, sql, inserts).then(result => {
            loopComments(db, result).then(done => {
                res.status(200).send(done);
            })
        }, reject => {
            console.log(reject);
            res.status(500).json({ 'msg': "Error in database" });
        })
    }
}

methods.getPost = function (db) {
    return function (req, res) {
        let pid = req.params.pid;
        let sql = "SELECT * FROM posts WHERE pid = ?";
        let inserts = [pid];

        dbQuery(db, sql, inserts).then(result => {
            getEmailFromUid(db, result[0].user).then(email => {
                result[0].user = email;
                res.status(200).send(result[0]);
            })
        }, reject => {
            console.log(reject);
            res.status(500).json({ 'msg': "Error in database" });
        })
    }
}

methods.getUser = function (db) {
    return function (req, res) {
        let uid = req.body.uid;
        let sql = "SELECT uid, email, userType FROM users WHERE uid = ?";
        let inserts = [uid];

        dbQuery(db, sql, inserts).then(result => {
            res.status(200).send(JSON.stringify(result));
        }, reject => {
            console.log(reject);
            res.status(500).send('Error in database operation.');
        })
    }
};

methods.upgradeUser = function (db) {
    return function (req, res) {
        if (req.auth) {
            let sql = 'SELECT userType FROM users WHERE email = ?'
            let inserts = req.auth.sub;
            let type;
            dbQuery(db, sql, inserts).then(valid => {
                console.log("test");
                if (valid[0].userType == 'admin' || valid[0].userType == 'moderator') {
                    console.log("test2");
                    if (req.body.mode == 'm') {
                        type = "'moderator'";
                    }
                    else if (req.body.mode == 'a') {
                        type = "'admin'";
                    }
                    sql = "UPDATE users SET userType = " + type + " WHERE uid = ?";
                    console.log(sql)
                    inserts = [req.body.uid]
                    dbQuery(db, sql, inserts).then(result => {
                        res.status(200).json({ 'msg': "Updated" });
                    }, reject => {
                        console.log(reject);
                        res.status(500).json({ 'msg': "Error in database" });
                    })
                }
                else {
                    res.status(401).json({ 'msg': 'You are not authorized to upgrade users' });
                }
            })
        }
        else {
            res.status(401).json({ 'msg': "You are not logged in" });
        }
    }
}

methods.deleteUser = function (db) {
    return function (req, res) {
        if (req.auth) {
            let sql = 'SELECT userType FROM users WHERE email = ?'
            let inserts = req.auth.sub;
            dbQuery(db, sql, inserts).then(valid => {
                if (valid[0].userType == 'admin' || valid[0].userType == 'moderator') {
                    sql = 'DELETE FROM users WHERE uid = ?'
                    inserts = [req.body.uid]
                    dbQuery(db, sql, inserts).then(result => {
                        res.status(200).json({ 'msg': "Deleted user" });
                    }, reject => {
                        console.log(reject);
                        res.status(500).json({ 'msg': "Error in database" });
                    })
                }
                else {
                    res.status(401).json({ 'msg': 'You are not authorized to see all users' });
                }
            })
        }
        else {
            res.status(401).json({ 'msg': "You are not logged in" });
        }
    }
}

methods.createUser = function (db) {
    return function (req, res) {
        if (req.body.password && req.body.email) {
            bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
                if (err) {
                    console.log(err)
                    res.status(500).send({ 'msg': "Could not add you" });
                }
                else {
                    let sql = "INSERT INTO users (email, password) VALUES (?, ?)"
                    let inserts = [req.body.email, hash];

                    dbQuery(db, sql, inserts).then(result => {

                        const token = jwt.sign({ sub: req.body.email }, privateKey, { algorithm: 'RS256' });

                        res.cookie('token', token, { maxAge: 900000, httpOnly: false, sameSite: "strict" });
                        res.status(200).send(JSON.stringify({ 'msg': "Congrats! You are now a user" }));

                    }, reject => {
                        res.status(401).send({ 'msg': "Couldn't add you, most likely you are already a user" });
                    })
                }
            });
        }
        else {
            res.status(400).send({ 'msg': "Please type something in all fields" });
        }
    };
}

methods.createPost = function (db) {
    return function (req, res) {
        if (req.body.title && req.body.content) {
            if (req.auth) {
                let sql = "SELECT uid FROM users WHERE email = ?";
                let inserts = [req.auth.sub];

                dbQuery(db, sql, inserts).then(user => {
                    sql = "INSERT INTO posts(user, title, content) VALUES(?, ?, ?)";
                    inserts = [user[0].uid, req.body.title, req.body.content];
                    dbQuery(db, sql, inserts).then(result => {
                        res.send(JSON.stringify({ 'msg': "Post is added" }));
                    }), reject => {
                        console.log(reject);
                        res.status(500).json({ 'msg': "Could not add post" });
                    };
                }, reject => {
                    console.log("Reject: " + reject);
                    res.status(500).json({ 'msg': "Could not add post" });
                })
            }
            else {
                res.status(401).json({ 'msg': "You are not logged in" });
            }
        }
        else {
            res.status(400).json({ 'msg': "Please type something in all fields" });
        }
    }
}

methods.createComment = function (db) {
    return function (req, res) {
        if (req.body.comment) {
            if (req.auth) {

                let sql = "SELECT uid FROM users WHERE email = ?";
                let inserts = [req.auth.sub];

                dbQuery(db, sql, inserts).then(user => {
                    sql = "INSERT INTO comments(post, user, comment) VALUES(?, ?, ?)";
                    inserts = [req.body.post, user[0].uid, req.body.comment];
                    dbQuery(db, sql, inserts).then(result => {
                        res.status(200).end();
                    }), reject => {
                        console.log(reject);
                        res.status(500).end();
                    }
                }, reject => {
                    console.log(reject);
                    res.status(500).end();
                })
            }
            else {
                res.status(401).end();
            }
        }
        else {
            res.status(100).end();
        }
    }
}

methods.login = function (db) {
    return function (req, res, next) {
        const email = req.body.email;
        const password = req.body.password;

        let sql = "SELECT uid, email, password FROM users WHERE email = ?";
        let inserts = [email];

        dbQuery(db, sql, inserts).then(result => {
            if (result[0]) {
                return bcrypt.compare(password, result[0].password).then((suc, err) => {
                    if (!suc) {
                        res.status(403).send({ "msg": "Invalid credentials" });
                    }
                    else {
                        const token = jwt.sign({ sub: email }, privateKey, { algorithm: 'RS256' });

                        res.cookie('token', token, { maxAge: 900000, httpOnly: false, sameSite: "strict" });
                        res.status(200).json({ "msg": "You are logged in" });
                    }
                });
            }
            else {
                res.status(400).send({ "msg": "Invalid credentials" });
            }
        });
    }
}


methods.auth = function () {
    return function (req, res, next) {

        const token = jwt.verify(req.cookies.token, publicKey, { algorithm: 'RS256' }, function (err, decoded) { // Verify the user cookie is signed by us
            if (err) {
                console.log("Failed to verify jwt token");
                console.log(err);
            }
            else if (decoded) {
                req.auth = decoded;
            }
            next();
        });
    }
}

function dbQuery(db, query, inserts) {
    return new Promise((resolve, reject) => {
        query = mysql.format(query, inserts); // Sanitize input
        db.query(query, function (err, result) {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

function getEmailFromUid(db, uid) {
    return new Promise((resolve, reject) => {
        let sql = "Select email FROM users WHERE uid = ?";
        let inserts = [uid];
        dbQuery(db, sql, inserts).then(res => {
            resolve(res[0].email);
        }), rej => {
            reject();
        }
    })

}

function loopComments(db, result) { // Get all comments in result
    return new Promise((resolve, reject) => {
        for (let i = 0; i < result.length; i++) {
            getEmailFromUid(db, result[i].user).then(email => {
                result[i].user = email;
                if (i === result.length - 1) {
                    resolve(result); // Only resolve when done
                }
            })
        }
    })
}